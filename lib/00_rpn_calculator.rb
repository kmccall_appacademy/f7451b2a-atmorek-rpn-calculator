class RPNCalculator
  attr_accessor :stack

  def initialize
    @stack = []
  end

  def value
    stack[-1]
  end

  def push(number)
    stack << number
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    string.split.map { |ch| operator?(ch) ? ch.to_sym : ch.to_i }
  end

  def evaluate(string)
    tokens = tokens(string)

    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end

    value
  end

  private

  def perform_operation(operator_sym)
    raise "calculator is empty" if stack.length < 2
    second_operator = stack.pop
    first_operator = stack.pop

    case operator_sym
    when :+
      stack << first_operator + second_operator
    when :-
      stack << first_operator - second_operator
    when :*
      stack << first_operator * second_operator
    when :/
      stack << first_operator.fdiv(second_operator)
    else
      raise "No such operator #{operator_sym}"
      stack << first_operator
      stack << second_operator
    end
  end

  def operator?(char)
    ["+", "-", "*", "/"].include?(char)
  end

end
